

public class Gauss {

	/**
	 * Diese Methode soll die Loesung x des LGS R*x=b durch
	 * Rueckwaertssubstitution ermitteln.
	 * PARAMETER: 
	 * R: Eine obere Dreiecksmatrix der Groesse n x n 
	 * b: Ein Vektor der Laenge n
	 */
	public static double[] backSubst(double[][] R, double[] b) {
		double x[] = new double[b.length];		
				
		int n = R.length-1;
		x[n] = 0.0;
		
		//zeilenweise von unten noch oben
		for(int i = n; i > -1;--i){ //i = zeile
			double sum = 0.0;
			for(int j = n;j-i > -1 && j > -1;--j){
				sum += R[i][j] * x[j];
			}
			x[i] = (b[i]-sum)/R[i][i];
		}
		return x;
	}

	/**
	 * Diese Methode soll die Loesung x des LGS A*x=b durch Gauss-Elimination mit
	 * Spaltenpivotisierung ermitteln. A und b sollen dabei nicht veraendert werden. 
	 * PARAMETER: A:
	 * Eine regulaere Matrix der Groesse n x n 
	 * b: Ein Vektor der Laenge n
	 */
	
	public static double[][] gauss(double [][] A, double []b){
		//copy A and b into one matrix
		int m = A.length; int n = A.length +1;
		double [][] a = new double [m][n];
		for(int i = 0;i<m;++i){
			for(int j = 0;j<m;++j){
				a[i][j] = A[i][j];
			}
			a[i][m] = b[i];
		}
		
		//spalten pivotsuche, b ausgeschlossen
		for(int j = 0;j<n-1;++j){//spalten
			//pivot finden
			int max_index =j ; double prev = Math.abs(a[j][j]);
			for(int i = j+1; i<m;++i){//zeilen
				if(Math.abs(a[i][j])>prev){
					prev = Math.abs(a[i][j]);
					max_index = i;
				}
			}
			//zeilen tauschen
			if(j != max_index) interchange(a,j,max_index);//max is now in jj
			//eliminieren
			if(a[j][j] == 0.0) continue;//avoid division by zero
			for(int x = j+1; x < m;++x){//zeilen
				double factor = a[x][j]/a[j][j]; 
				if(factor == 0.0) continue;// would not have any effect
				for(int y = j;y<n;++y){//spalten
					//System.out.print(a[x][y] + "-(" + factor +"*"+a[j][y] +")=");
					a[x][y] -= factor * a[j][y];
					//System.out.print(a[x][y] + "\n");
				}
			}
		}
		return a;
	}
	
	public static double[] solve(double[][] A, double[] b) {
		int m = A.length;
		double [][] a = gauss(A,b);
		//Extract matrix and solution vector b
		double [][] R = new double [m][m];
		double[] rB = new double[m];
		for(int i = 0;i<m;++i){
			for(int j = 0;j<m;++j){
				R[i][j] = a[i][j];
			}
			rB[i] = a[i][m];
		}
		
		return backSubst(R, rB);
	}
	
	public static void interchange(double [][] A, int from,int to){
		double tmp;
		//tausche zeilen i und j
		for(int i = 0; i<A[0].length;++i){
			tmp = A[from][i];
			A[from][i] = A[to][i];
			A[to][i] = tmp;
		}		
	}

	/**
	 * Diese Methode soll eine Loesung p!=0 des LGS A*p=0 ermitteln. A ist dabei
	 * eine nicht invertierbare Matrix. A soll dabei nicht veraendert werden.
	 * 
	 * Gehen Sie dazu folgendermassen vor (vgl.Aufgabenblatt): 
	 * -Fuehren Sie zunaechst den Gauss-Algorithmus mit Spaltenpivotisierung 
	 *  solange durch, bis in einem Schritt alle moeglichen Pivotelemente
	 *  numerisch gleich 0 sind (d.h. <1E-10) 
	 * -Betrachten Sie die bis jetzt entstandene obere Dreiecksmatrix T und
	 *  loesen Sie Tx = -v durch Rueckwaertssubstitution 
	 * -Geben Sie den Vektor (x,1,0,...,0) zurueck
	 * 
	 * Sollte A doch intvertierbar sein, kann immer ein Pivot-Element gefunden werden(>=1E-10).
	 * In diesem Fall soll der 0-Vektor zurueckgegeben werden. 
	 * PARAMETER: 
	 * A: Eine singulaere Matrix der Groesse n x n 
	 */
	public static double[] solveSing(double[][] A) {	
		double [][] a = gauss(A,new double[A.length]);
		//Util.printMatrix(a);
		//obere rechte dreieck matrix finden
		int m = a.length;
		int nullJ = -1;
		for(int j = 0; j < m; ++j){
			int counter = 0;
			for(int i = 0; i < m; ++i){
				if(Math.abs(a[i][j]) < 1E-10){
					++counter;
				}
			}
			if(counter > m-j-1){
				nullJ = j;
				break;
			}
		}
		if(nullJ == -1){
			throw new ArithmeticException("Not a singular matrix!");
		}
		//T extrahieren
		double [][] T = new double[nullJ][nullJ];
		for(int i = 0; i<nullJ; ++i){
			for(int j = 0; j< nullJ; ++j){
				T[i][j] = a[i][j];
			}
		}
		//v extrahieren
		double [] v = new double[nullJ];
		for(int i = 0; i < nullJ; ++i){
			v[i] = -a[i][nullJ];
		}
		//Tx = -v l�sen
		v = backSubst(T, v);
		//p(x,1.0,0...)
		double [] p = new double[m];
		for(int i = 0; i<nullJ; ++i){
			p[i] = v[i];
		}
		p[nullJ] = 1.0;
		//Util.printVector(p);
		//Util.printMatrix(a);
		//Util.printMatrix(T);
		//Util.printVector(matrixVectorMult(a, p)); // Yay it works
		return p;
	}

	/**
	 * Diese Methode berechnet das Matrix-Vektor-Produkt A*x mit A einer nxm
	 * Matrix und x einem Vektor der Laenge m. Sie eignet sich zum Testen der
	 * Gauss-Loesung
	 */
	public static double[] matrixVectorMult(double[][] A, double[] x) {
		int n = A.length;
		int m = x.length;

		double[] y = new double[n];

		for (int i = 0; i < n; i++) {
			y[i] = 0;
			for (int j = 0; j < m; j++) {
				y[i] += A[i][j] * x[j];
			}
		}

		return y;
	}
}
